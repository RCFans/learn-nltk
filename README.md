# Learn NLTK #

NLTK is a leading platform for building Python programs to work with human language data.

### How to install NLTK? ###

* [Install NLTK](http://www.nltk.org/install.html)
* [Install NLTK Data](http://www.nltk.org/data.html)

### Lessons ###

* [Stemming Words](http://nbviewer.ipython.org/urls/bitbucket.org/RCFans/learn-nltk/raw/63a7d6e722329f7df8588db13e8a64bc1bcf0604/Stemming%20words.ipynb)
* [Search Term Analysis](http://nbviewer.ipython.org/urls/bitbucket.org/RCFans/learn-nltk/raw/60639bc3b5fc05d3ffbb71634e3e474484d322f3/Search%20Term%20Analysis.ipynb)